package bd.edu.seu.icpcteamvalidation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IcpcTeamValidationApplication {

    public static void main(String[] args) {
        SpringApplication.run(IcpcTeamValidationApplication.class, args);
    }

}
