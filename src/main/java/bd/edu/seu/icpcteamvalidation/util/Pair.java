package bd.edu.seu.icpcteamvalidation.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = {"first", "second"})
public class Pair<T, U> {
    private T first;
    private U second;

    public static <T, U> Pair<T, U> of(T t, U u) {
        return new Pair(t, u);
    }
}