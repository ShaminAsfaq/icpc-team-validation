package bd.edu.seu.icpcteamvalidation.model;

public enum Status {
    ACCEPTED,
    CANCELED,
    PENDING
}
