package bd.edu.seu.icpcteamvalidation.model;

import bd.edu.seu.icpcteamvalidation.util.Pair;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
public class Team {
    private int tableId;
    private String teamUrl;
    private String instituteName;
    private String teamName;
    private Pair<String, String> coach;
    private List<Pair<String, String>> contestantList;
    private Status status;
    private List<String> logList;

    public Team() {
        contestantList = new ArrayList<>();
        logList = new ArrayList<>();
    }
}
