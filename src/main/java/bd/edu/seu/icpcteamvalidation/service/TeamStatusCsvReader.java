package bd.edu.seu.icpcteamvalidation.service;

import bd.edu.seu.icpcteamvalidation.model.Status;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

@Service
public class TeamStatusCsvReader {
    private final static Map<String, Status> teamStatusMap;

    static {
        teamStatusMap = new HashMap<>();
        try {
            Files.lines(Paths.get("team-status.csv"))
                    .forEach(line -> {
                        String[] tokens = line.split("\\,");
                        teamStatusMap.put(tokens[0], Status.valueOf(tokens[1]));
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Status getStatusOf(String teamName) {
        return teamStatusMap.get(teamName);
    }
}
