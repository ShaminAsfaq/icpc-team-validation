package bd.edu.seu.icpcteamvalidation.service;

import bd.edu.seu.icpcteamvalidation.configuration.SeleniumConfig;
import bd.edu.seu.icpcteamvalidation.model.Status;
import bd.edu.seu.icpcteamvalidation.model.Team;
import bd.edu.seu.icpcteamvalidation.util.Pair;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.opencsv.CSVWriter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

@Service
public class LoginService {
    @Value("${baylor.username}")
    private String username;
    @Value("${baylor.password}")
    private String password;
    @Value("${baylor.maxteams}")
    int teamMaxLimit;

    @Value("${baylor.start.index}")
    int startIndex;
    @Value("${baylor.end.index}")
    int endIndex;

    private SeleniumConfig seleniumConfig;
    private TeamStatusCsvReader teamStatusCsvReader;

    public LoginService(SeleniumConfig seleniumConfig, TeamStatusCsvReader teamStatusCsvReader) {
        this.seleniumConfig = seleniumConfig;
        this.teamStatusCsvReader = teamStatusCsvReader;
    }

    public void doLogin() throws Exception {
        WebDriver driver = seleniumConfig.getDriver();

        driver.get("https://icpc.baylor.edu/private");

        Thread.sleep(5000);

        String title = driver.getTitle();
        assert title.equals("Log in to the application");
        System.out.println(title);

        WebElement usernameField = driver.findElement(By.id("username"));
        usernameField.clear();
        usernameField.sendKeys(username);

        WebElement passwordField = driver.findElement(By.id("password"));
        passwordField.clear();
        passwordField.sendKeys(password);
        passwordField.sendKeys(Keys.RETURN);

        Thread.sleep(5000);

        assert driver.getTitle() != null;
    }

    public List<Team> visitPreliminaryContestTeamsPage() throws Exception {
        //  Initiating Driver
        WebDriver driver = seleniumConfig.getDriver();

        //  URL to List containing all the teams.
        String teamListUrl = "https://icpc.baylor.edu/cm5-contest/contest/Dhaka-Preliminary-2019/teams?dswid=733";
        driver.get(teamListUrl);

        //  Manipulating DOM element to set the HTML "value" of a dropdown option.
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById('allTeamsForm:allTeamsTable:j_id2').childNodes[0].setAttribute('value', '" + teamMaxLimit + "')");

        //  Selecting the max number of teams to load in a single page.
        Select dropSelect = new Select(driver.findElement(By.id("allTeamsForm:allTeamsTable:j_id2")));
        dropSelect.selectByValue(teamMaxLimit + "");

        //  Waiting for all the team names to load in a single screen. Time: 30 seconds, intuitively set.
        Thread.sleep(teamMaxLimit * 10);

        //  Inserting all team URL to a single array list.
        List<String> teamUrlList = new ArrayList<>();
        for (int i = 0; i < teamMaxLimit; i++) {
            try {
                WebElement anchor = driver.findElement(By.name(String.format("allTeamsForm:allTeamsTable:%d:teamCertificationLinkGET", i)));
                String href = anchor.getAttribute("href").replaceAll("certification", "");
                teamUrlList.add(href);
            } catch (Exception ex) {
                break;
            }
        }

        //  Printing total number of teams.
        System.out.println("Total teams found: " + teamUrlList.size());
//        teamUrlList.forEach(System.out::println);

        List<Team> teamList = new ArrayList<>();

        //  Number of teams after which the test should stop.
        for (int i = startIndex; i <= endIndex; i++) {

            try {
                String href = teamUrlList.get(i).replaceAll("certification", "");
//            System.out.printf("%d [%s]\n", i, href);

                // visit individual team page
                driver.get(href);

                //  Waiting for 3 seconds to load the page.
                Thread.sleep(3000);

                //  Clicking Eligibility button
                WebElement eligibilityButton = driver.findElement(By.xpath("//span[contains(text(),'Eligibility:')]"));
                eligibilityButton.click();

                //  Clicking Revalidate button in the pop up window.
                WebElement revalidateButton = driver.findElement(By.xpath("//span[@class='jss199'][contains(text(),'Revalidate')]"));
                revalidateButton.click();

                //  Waiting for 4 seconds to complete the re-validation before clicking CLOSE button.
                Thread.sleep(4000);

                Team team = new Team();
                teamList.add(team);

                team.setStatus(Status.PENDING);
                team.setTableId(i);
                team.setTeamUrl(href);

                WebElement teamNameField = driver.findElement(By.xpath("//input[@id='team']"));
                String teamName = teamNameField.getAttribute("value");
                team.setTeamName(teamName);

                WebElement institutionNameField = driver.findElement(By.xpath("//input[@placeholder='Type at least 3 letters...']"));
                String institutionName = institutionNameField.getAttribute("value");
                team.setInstituteName(institutionName);

                WebElement eligibilityTextField = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/main[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/div[4]/span[1]/button[1]/span[1]"));
                String eligibilityText = eligibilityTextField.getText();
                if (eligibilityText.contains("PREDICTED ELIGIBLE"))
                    team.setStatus(Status.ACCEPTED);

                if (team.getStatus() == Status.PENDING) {
                    Status csvStatus = teamStatusCsvReader.getStatusOf(team.getTeamName());

                    if (csvStatus != null && csvStatus == Status.ACCEPTED) {
                        System.err.println(team.getTeamName() + " " + team.getTeamUrl() + " was accepted before!");
                    }

                    try {
                        // TODO can't read log yet
                        List<WebElement> logElements = driver.findElements(By.className("jss361 jss368 jss440 jss442"));
                        logElements.forEach(logElement -> team.getLogList().add(logElement.getText()));
                    } catch (NoSuchElementException e) {
                        System.err.println("Team status is pending but log elements are missing for " + team.getTeamName());
                        //System.err.println(e.getMessage());
                    }
                }

                try {
                    WebElement coachEmailField = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/main[1]/div[1]/div[3]/div[1]/div[1]/div[1]/ul[1]/li[1]/div[2]/p[1]"));
                    String coachEmail = coachEmailField.getText();
                    WebElement coachNameField = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/main[1]/div[1]/div[3]/div[1]/div[1]/div[1]/ul[1]/li[1]/div[2]/span[1]"));
                    String coachName = coachNameField.getText();
                    team.setCoach(Pair.of(coachName, coachEmail));
                } catch (NoSuchElementException e) {
                    System.err.println("Coach missing for " + team.getTeamName());
                    //System.err.println(e.getMessage());
                }

                System.out.println(team.getTableId() + " " + team.getTeamName() + " " + team.getTeamUrl() + " " + teamStatusCsvReader.getStatusOf(team.getTeamName()) + " -> " + team.getStatus());
                for (int member = 1; member <= 3; member++) {
                    try {
                        WebElement teamMemberNameField = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/main[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[" + member + "]/td[1]/div[1]/span[2]/p[1]"));
                        String teamMemberName = teamMemberNameField.getText();
                        WebElement teamMemberEmailField = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/main[1]/div[1]/div[4]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[" + member + "]/td[2]"));
                        String teamMemberEmail = teamMemberEmailField.getText();

//                    System.out.printf("Name  [%s][%s]\n", teamMemberName, teamMemberNameField);
//                    System.out.printf("Email [%s][%s]\n", teamMemberEmail, teamMemberEmailField);

                        team.getContestantList().add(Pair.of(teamMemberName, teamMemberEmail));
                    } catch (NoSuchElementException e) {
//                    System.err.printf("Team member %d missing for %s\n", member, team.getTeamName());
                        //System.err.println(e.getMessage());
                    }
                }
//            System.out.println();

                WebElement close = driver.findElement(By.xpath("//span[contains(text(),'close')]"));
                close.click();

                //  Waiting 2 seconds before going into another round of the loop.
                Thread.sleep(2000);
            } catch (Exception ex) {
                break;
            }
        }

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        String output = objectMapper.writeValueAsString(teamList);

        RandomAccessFile outputFile = new RandomAccessFile("teams.json", "rw");
        outputFile.setLength(0);
        outputFile.writeBytes(output);

        RandomAccessFile acceptedTeamsFile = new RandomAccessFile("accepted-teams.txt", "rw");
        acceptedTeamsFile.setLength(0);
        teamList
                .stream()
                .filter(team -> team.getStatus() == Status.ACCEPTED)
                .forEach(team -> {
                    try {
                        acceptedTeamsFile.writeBytes(team.getTeamName() + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });

        return teamList;
    }

    public void visitTeamListPageToAccept(List<Team> teamList) throws Exception {
        //  Initiating Driver
        WebDriver driver = seleniumConfig.getDriver();

        //  URL to List containing all the teams.
        String teamListUrl = "https://icpc.baylor.edu/cm5-contest/contest/Dhaka-Preliminary-2019/teams?dswid=733";
        driver.get(teamListUrl);

        //  Manipulating DOM element to set the HTML "value" of a dropdown option.
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementById('allTeamsForm:allTeamsTable:j_id2').childNodes[0].setAttribute('value', '" + teamMaxLimit + "')");

        //  Selecting the max number of teams to load in a single page.
        Select dropSelect = new Select(driver.findElement(By.id("allTeamsForm:allTeamsTable:j_id2")));
        dropSelect.selectByValue(teamMaxLimit + "");

        //  Waiting for all the team names to load in a single screen. Time: 30 seconds, intuitively set.
        Thread.sleep(teamMaxLimit * 10);

        teamList.forEach(team -> {
            try {
                WebElement teamRow = driver.findElement(By.partialLinkText(team.getTeamName())).findElement(By.xpath("./..")).findElement(By.xpath("./.."));
                int rowIndex = Integer.parseInt(teamRow.getAttribute("data-ri"));
                //System.out.println(team.getTeamName() + " " + team.getStatus());

                if (team.getStatus() == Status.ACCEPTED) {
                    try {
                        String id = String.format("allTeamsForm:allTeamsTable:%d:j_idt212", rowIndex);
                        WebElement acceptButton = driver.findElement(By.id(id));
                        acceptButton.click();
                        System.out.println("Accepted " + team.getTeamName());
                    } catch (NoSuchElementException e) {
                        System.err.println("Accept button missing for " + team.getTeamName() + " " + team.getTeamUrl());
                    } catch (StaleElementReferenceException e) {
                        System.err.println("Stale element for " + team.getTeamName() + " " + team.getTeamUrl());
                    } catch (Exception e) {
                        System.err.println("Something else wrong for " + team.getTeamName() + " " + team.getTeamUrl());
                    }
                }
            } catch (NoSuchElementException e) {
                System.err.println("Row missing for " + team.getTeamName() + " " + team.getTeamUrl());
            } catch (StaleElementReferenceException e) {
                System.err.println("Stale element for " + team.getTeamName() + " " + team.getTeamUrl());
            } catch (Exception e) {
                System.err.println("Something else wrong for " + team.getTeamName() + " " + team.getTeamUrl());
            }
        });
    }

    public void closeBrowsers() {
        WebDriver driver = seleniumConfig.getDriver();
        driver.quit();
    }

    /**
     * Intended to parse teams.json file and retrieve coach information
     */
    public static void parseJSON() {
        System.out.println("I AM HERE...");
        JSONParser jsonParser = new JSONParser();
        try {
            FileReader fileReader = new FileReader("CombinedJSON.json");
            Object obj = jsonParser.parse(fileReader);

            JSONArray teamList = (JSONArray) obj;
            List<Team> foundTeamList = new ArrayList<>();

            teamList.forEach(team -> {
                JSONObject jsonObject = (JSONObject) team;
                JSONObject coach = (JSONObject) jsonObject.get("coach");

                String teamUrl = (String) jsonObject.get("teamUrl");
                teamUrl = teamUrl.replaceAll("https://icpc.baylor.edu/private/teams/", "");
                teamUrl = teamUrl.replaceAll("/", "");

                Team foundTeam = new Team();
                foundTeam.setTeamUrl(teamUrl);

                Pair<String, String> foundCoach = new Pair<>();
                foundCoach.setFirst((String) coach.get("first"));
                foundCoach.setSecond((String) coach.get("second"));

                foundTeam.setCoach(foundCoach);
                foundTeamList.add(foundTeam);
            });
//            foundTeamList.forEach(System.out::println);

            /**
             * Writing CSV with Team ID, Coach Name and E-mail and Incomplete Message
             */
            String outCsv = "TeamIdAndCoach.csv";
            CSVWriter writer = new CSVWriter(new FileWriter(outCsv));


            foundTeamList.forEach(team -> {
                String[] line = new String[4];
                line[0] = team.getTeamUrl();
                line[1] = team.getCoach().getFirst();

                String coachName = line[1].substring(6);
                line[1] = coachName;
                System.out.println(coachName + " is COACH of " + team.getTeamUrl());


                line[2] = team.getCoach().getSecond();

                if (line[2].contains("Registration")) {
                    line[3] = "Registration incomplete";
                    String[] faultyCoach = line[2].split(",");
                    line[2] = faultyCoach[0];
                    System.out.println("------> " + faultyCoach[0] + "----> " + faultyCoach[1]);
                }

                writer.writeNext(line);
            });
            writer.close();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
