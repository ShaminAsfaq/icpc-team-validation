package bd.edu.seu.icpcteamvalidation.service;

import bd.edu.seu.icpcteamvalidation.model.Team;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LoginServiceTest {
    @Autowired
    private LoginService loginService;

    @Test
    public void testLogin() {
        try {
            loginService.doLogin();
            List<Team> teamList = loginService.visitPreliminaryContestTeamsPage();
            loginService.visitTeamListPageToAccept(teamList);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            loginService.closeBrowsers();
        }
    }

    @Test
    public void testJSON() {
        LoginService.parseJSON();
    }
}

