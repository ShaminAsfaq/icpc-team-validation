package bd.edu.seu.icpcteamvalidation.service;

import bd.edu.seu.icpcteamvalidation.model.Status;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TeamStatusCsvReaderTest {
    @Autowired
    private TeamStatusCsvReader teamStatusCsvReader;

    @Test
    public void test_DIU_VITORIANS() {
        String teamName = "DIU_ VITORIANS";
        assertEquals(Status.ACCEPTED, teamStatusCsvReader.getStatusOf(teamName));
    }

    @Test
    public void test_UIU_Black_Hat() {
        String teamName = "UIU Black Hat";
        assertEquals(Status.PENDING, teamStatusCsvReader.getStatusOf(teamName));
    }
    @Test
    public void test_JUST_Return_To_Sleep() {
        String teamName = "JUST_Return_To_Sleep";
        assertEquals(Status.CANCELED, teamStatusCsvReader.getStatusOf(teamName));
    }
}
