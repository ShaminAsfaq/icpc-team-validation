# README #

### What is this repository for? ###

* Mainly, team verification from baylor site. 
* Parsing the data of all the coaches.

### How do I get set up? ###

* You will need Spring Boot
* You can use the extension Chropath to update the project for your own purposes easily.
* The necessary drivers are already provided within the project.
* You have to provide the baylor username and password in the application properties file to login. File address in the following bullet.
* Properties file: /src/main/resources/application.properties
* If everything is set, run the "testLogin()" method form the file LoginServiceTest.java the destination to which is give in the next bullet.
* Test file: /src/test/java/bd/edu/seu/icpcteamvalidation/service